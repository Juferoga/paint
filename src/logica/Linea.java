/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 *
 * @author Estudiantes
 */
public class Linea extends Figura{
    
    public Linea(Point posicion_i,Point posicion_f, Color c) {
        super(posicion, c);
    }

    @Override
    public void dibujar(Graphics g) {
        g.setColor(this.getC());
        g.drawLine(this.getPosicion().x-radio, this.getPosicion().y-radio, this.getPosicion().x-radio, this.getPosicion().y-radio );
        g.setColor(Color.black);
    }
    
}
