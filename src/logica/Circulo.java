/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 *
 * @author Estudiantes
 */
public class Circulo extends Figura{
    
    private int radio;
    
    public Circulo(Point posicion, Color c, int radio) {
        super(posicion, c);
        this.radio = radio;
    }

    /**
     * @return the radio
     */
    public int getRadio() {
        return radio;
    }

    /**
     * @param radio the radio to set
     */
    public void setRadio(int radio) {
        this.radio = radio;
    }

    @Override
    public void dibujar(Graphics g) {
        g.setColor(this.getC());
        g.drawOval(this.getPosicion().x-radio, this.getPosicion().y-radio, 2*radio, 2*radio);
        g.setColor(Color.black);
    }
    
}
