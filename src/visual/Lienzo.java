/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visual;

import controlador.Driver;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Estudiantes
 */
public class Lienzo extends JPanel {

    public Lienzo() {
        this.setBackground(Color.white);
        
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Driver.dibujarFiguras(g);
    }
    
    
    
}
