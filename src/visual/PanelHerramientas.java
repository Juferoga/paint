/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visual;

import controlador.Driver;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;

/**
 *
 * @author Estudiantes
 */
public class PanelHerramientas extends JPanel {

    private JButton borrador;
    private JButton lapiz;
    private JButton colores;
    
    public PanelHerramientas() {
        borrador = new JButton("Borrador");
        borrador.addActionListener(Driver.escuchador);
        lapiz = new JButton("Lapiz");
        lapiz.addActionListener(Driver.escuchador);
        colores = new JButton("Color");
        colores.addActionListener(Driver.escuchador);
        this.setLayout(new FlowLayout(FlowLayout.RIGHT));
        this.add(borrador);
        this.add(lapiz);
        this.add(colores);
        
    }

    /**
     * @return the borrador
     */
    public JButton getBorrador() {
        return borrador;
    }

    /**
     * @param borrador the borrador to set
     */
    public void setBorrador(JButton borrador) {
        this.borrador = borrador;
    }

    /**
     * @return the lapiz
     */
    public JButton getLapiz() {
        return lapiz;
    }

    /**
     * @param lapiz the lapiz to set
     */
    public void setLapiz(JButton lapiz) {
        this.lapiz = lapiz;
    }

    /**
     * @return the colores
     */
    public JButton getColores() {
        return colores;
    }

    /**
     * @param colores the colores to set
     */
    public void setColores(JButton colores) {
        this.colores = colores;
    }
    
}
