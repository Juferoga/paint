/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visual;

import controlador.Driver;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Estudiantes
 */
public class Ventana extends JFrame{

    private PanelFiguras pf;
    private PanelHerramientas ph;
    private Lienzo lienzo;
    
    public Ventana() {
        this.setBounds(0, 0, 600, 400);
        pf = new PanelFiguras();
        ph = new PanelHerramientas();
        lienzo = new Lienzo();
        lienzo.addMouseListener(Driver.escuchador);
        this.setLayout(new BorderLayout());
        JPanel p = new JPanel(new FlowLayout());
        p.add(pf);
        p.add(ph);
        this.add(p, BorderLayout.NORTH);
        this.add(lienzo, BorderLayout.CENTER);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * @return the pf
     */
    public PanelFiguras getPf() {
        return pf;
    }

    /**
     * @param pf the pf to set
     */
    public void setPf(PanelFiguras pf) {
        this.pf = pf;
    }

    /**
     * @return the ph
     */
    public PanelHerramientas getPh() {
        return ph;
    }

    /**
     * @param ph the ph to set
     */
    public void setPh(PanelHerramientas ph) {
        this.ph = ph;
    }

    /**
     * @return the lienzo
     */
    public Lienzo getLienzo() {
        return lienzo;
    }

    /**
     * @param lienzo the lienzo to set
     */
    public void setLienzo(Lienzo lienzo) {
        this.lienzo = lienzo;
    }
    
}
