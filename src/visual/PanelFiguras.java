/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visual;

import controlador.Driver;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Estudiantes
 */
public class PanelFiguras extends JPanel{
    
    private JButton cuadrado;
    private JButton circulo;
    private JButton triangulo;
    private JButton linea;

    public PanelFiguras() {
        cuadrado = new JButton("Cuadrado");
        circulo = new JButton("Circulo");
        circulo.addActionListener(Driver.escuchador);
        triangulo = new JButton("Triangulo");
        linea = new JButton("Linea");
        setLayout(new FlowLayout(FlowLayout.LEFT));
        this.add(cuadrado);
        this.add(circulo);
        this.add(triangulo);
        this.add(linea);
    }

    /**
     * @return the cuadrado
     */
    public JButton getCuadrado() {
        return cuadrado;
    }

    /**
     * @param cuadrado the cuadrado to set
     */
    public void setCuadrado(JButton cuadrado) {
        this.cuadrado = cuadrado;
    }

    /**
     * @return the circulo
     */
    public JButton getCirculo() {
        return circulo;
    }

    /**
     * @param circulo the circulo to set
     */
    public void setCirculo(JButton circulo) {
        this.circulo = circulo;
    }

    /**
     * @return the triangulo
     */
    public JButton getTriangulo() {
        return triangulo;
    }

    /**
     * @param triangulo the triangulo to set
     */
    public void setTriangulo(JButton triangulo) {
        this.triangulo = triangulo;
    }

    /**
     * @return the linea
     */
    public JButton getLinea() {
        return linea;
    }

    /**
     * @param linea the linea to set
     */
    public void setLinea(JButton linea) {
        this.linea = linea;
    }
    
}
