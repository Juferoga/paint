/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import logica.Figura;
import visual.Ventana;

/**
 *
 * @author Estudiantes
 */
public class Driver  {
    
    public static Ventana miventana;
    public static Escuchador escuchador = new Escuchador();
    public static Color colorSeleccionado = Color.BLACK;
    
    public static ArrayList<Figura> listaFiguras = new ArrayList<Figura>();

    public static void dibujarFiguras(Graphics g) {
        for (Figura listaFigura : listaFiguras) {
            listaFigura.dibujar(g);
        }
    }
    
    
    
}
