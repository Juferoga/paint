/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import logica.Circulo;
import logica.linea;

/**
 *
 * @author Estudiantes
 */
 
public class Escuchador implements ActionListener,MouseListener{
    
    Point guardarDatoInicial;
    int tipoFigura = 0;

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(Driver.miventana.getPh().getColores())){
            JColorChooser chooser = new JColorChooser();
        chooser.setColor(Color.BLUE);
        chooser.getSelectionModel().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent arg0) {
                Driver.colorSeleccionado = chooser.getColor();
                System.out.println(Driver.colorSeleccionado);
            }
        });
        JDialog dialog = JColorChooser.createDialog(null, "Color Chooser",
                true, chooser, null, null);
        dialog.setVisible(true);
        }
        if(e.getSource().equals(Driver.miventana.getPf().getCirculo())){
            tipoFigura = 2;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //System.out.println(e.getPoint());
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(tipoFigura!=0){
            guardarDatoInicial = e.getPoint();
        }
        System.err.println(e.getPoint());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(tipoFigura==2){
            Driver.listaFiguras.add(new Circulo(guardarDatoInicial, Driver.colorSeleccionado, (int)e.getPoint().distance(guardarDatoInicial)));            
            Driver.miventana.getLienzo().repaint();
        }
        else{
            Driver.listaFiguras.add(new Linea(guardarDatoInicial,  Driver.colorSeleccionado, (int)e.getPoint().distance(guardarDatoInicial)));
        }
        
        System.out.println(e.getPoint());
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }
    
}
